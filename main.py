import json
import sys

import yaml
import os
import subprocess
import argparse
import requests
from apscheduler.triggers.cron import CronTrigger
from loguru import logger
from apscheduler.schedulers.blocking import BlockingScheduler

logger.add(
    "logs/{time:YYYYMMDD}.log",
    format="[{time:YYYY-MM-DD HH:mm:ss}][{level}] 文件：{file}，行数：{line}，方法：{function}，信息：{message}，异常：{exception}"
)


def parse_ports_info(filename: str) -> list:
    with open(filename, 'r') as stream:
        data = yaml.safe_load(stream)
        return data


def scan(scanner_path: str, host: str) -> subprocess.CompletedProcess:
    result_file = 'result.json'
    if os.path.exists(result_file):
        os.remove(result_file)

    scanner = os.path.join(scanner_path, 'naabu')
    cmd = '%s -host %s -port - -json -o result.json -c 800 -rate 10000 -verify' % (scanner, host)
    logger.info('执行扫描命令：%s' % cmd)
    return subprocess.run(cmd, shell=True, capture_output=False)


def send_notify(key: str, content: str):
    logger.info('通知内容：%s' % content)
    hook_url = 'https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=%s' % key
    data = {
        'msgtype': 'text',
        'text': {
            'content': content,
            'mentioned_list': ['@all']
        }
    }
    response = requests.post(url=hook_url, json=data)
    if response.status_code != 200:
        logger.error('请求发送接口失败，响应码：%d', response.status_code)
    else:
        response_json = response.json()
        if response_json.get('errcode') != 0:
            logger.error('接口返回错误：%s' % response_json.get('errmsg'))
        else:
            logger.info('发送通知成功！')


parser = argparse.ArgumentParser()
parser.add_argument('-p', '--platform', help='程序运行的平台，默认为linux', default='linux', type=str)
parser.add_argument('-k', '--key', help='企业微信群机器人WebHook Key', default='', type=str)
parser.add_argument('-c', '--cron', help='cron表达式', default='0 */2 * * *', type=str)


def main(key: str, platform: str):
    scanner_path = os.path.join('./naabu', platform)
    ports_info = parse_ports_info('ports.yaml')
    if len(ports_info) == 0:
        logger.error('请先配置要扫描主机及端口白名单。')
    else:
        hosts = []
        host_list = {}
        for i in ports_info:
            hosts.append(i['host'])
            host_list[i['host']] = i['port_whitelist']
        host_str = ','.join(hosts)
        logger.info('开始扫描')
        ret = scan(scanner_path=scanner_path, host=host_str)
        if ret.returncode == 0:
            logger.info('扫描完毕')
            if os.path.exists('result.json'):
                fp = open('result.json', 'r')
                result_str = ','.join(fp.readlines())
                result_json = '[%s]' % result_str
                result_list = json.loads(result_json)
                lake = {}
                for j in result_list:
                    if j['ip'] in host_list:
                        if j['port'] not in host_list[j['ip']]:
                            if j['ip'] in lake:
                                lake[j['ip']].append(j['port'])
                            else:
                                lake[j['ip']] = [j['port']]
                if len(lake) == 0:
                    logger.info('扫描完毕，未发现泄露端口')
                else:
                    notify_str = '发现存在可能泄露的端口：\n'
                    for k, v in lake.items():
                        lake_info = '主机：%s\n端口：%s\n' % (k, ','.join('%s' % port for port in v))
                        notify_str += lake_info
                    send_notify(key=key, content=notify_str)
        else:
            logger.error('扫描失败：%s' % ret.stderr.decode('utf8').strip())


if __name__ == '__main__':
    args = parser.parse_args()
    cron = args.cron
    key = args.key
    platform = args.platform
    if key == '':
        print('请设置企业微信群机器人的WebHook的key')
        sys.exit(0)
    if cron == '':
        cron = '0 */2 * * *'
        print('未设置Cron表达式，使用默认值')
    logger.info('程序已成功运行')
    schedule = BlockingScheduler()
    schedule.add_job(main, trigger=CronTrigger.from_crontab(cron), id='scan_port', args=(key, platform,))
    schedule.start()
